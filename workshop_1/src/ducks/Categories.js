const SELECT = 'category/select'

export const selectCategory = payload => ({
    type: SELECT,
    payload, // id
})

const initialState = {
    data: [
        { id: 1, name: 'Defecto', selected: true },
        { id: 2, name: 'Deportes', selected: false },
        { id: 3, name: 'Tecnología', selected: false },
    ],
}

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case SELECT: {
        return {
            ...state,
            data: state.data.map(x => ({
            ...x,
            selected: x.id === action.payload,
            }))
        }
        }
        default:
        return state
    }
}