const ADD_NEWS = 'news/add'

const initialState = {
    data: [],
}

export const addNews = payload => ({
    type: ADD_NEWS,
    payload,
})


export default function reducer(state = initialState, action) {
    switch(action.type) {
        case ADD_NEWS: {
            return {
                ...state,
                data: state.data.concat(action.payload),
            }
        }
        default:
            return state
    }
}
