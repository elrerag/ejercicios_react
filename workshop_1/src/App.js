import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NewsForm, CategoriesList} from './components'
import { NewsFeed } from './components/NewsFeed'

class App extends Component {
    render() {
        return (
        <div>
            <div>
                <NewsForm />
            </div>
            <div>
                <CategoriesList />
                <NewsFeed />
            </div>
        </div>
        )
    }
}

const mapStateToProps = state => {
    return state
}

export default connect(
    mapStateToProps,
    // mapDispatchToProps,
)(App)
