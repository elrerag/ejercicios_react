import React, { Component } from 'react';
import './App.css';
import { reducer as formReducer } from redux-form

class App extends Component {

    handleSubmit = e => {

        e.preventDefault()
        
    }
    render() {
        const {errors} = this.props
        return (
        <div className="App">
            <UserForm />
            <input type="text" name="name" placeholder="Nombre"/>
            {errors.name}
            <input type="text" name="lastname" placeholder="Apellido"/>
            {errors.lastname}
        </div>
        );
    }
    }

export default App;
