import React, { Component } from 'react';
import 'bulma/css/bulma.css'
import './App.css';

class Box extends Component {

    render(){

        const { box_data } = this.props

        return (
            <div className="box">
            <article className="media">
                <div className="media-left">
                    <figure className="image is-60x90">
                        <img alt="" with="" height="" src={ box_data.photo } />
                    </figure>
                </div>
                <div className="media-content">
                    <div className="content">
                        <p>
                            <strong>{ box_data.author }</strong>
                            <small>{ box_data.author_email }</small><br /> 
                            { box_data.author_msj }
                        </p>
                    </div>

                </div>
            </article>
        </div>
        )
    }
}


class ImputText extends Component {
    constructor(){
        super()

        this.state = {
            value: '',
        }
    }

    maneja_value(event){
        if(event.key==="Enter"){
            const { add_box  } = this.props
            add_box(event)
            event.target.value = ''
        }

    }

    render(){

        return (
            <div className="field">
                <label className="label">El posteador</label>

                <div className="control">
                    <input onKeyPress={ this.maneja_value.bind(this) }
                    className="input"
                    type="text"
                    placeholder="Ingrese su texto y presione enter..."
                    
                />

                </div>
            </div>
        )

    }
}

class App extends Component {
    constructor(){
        super()

        this.state = {
            list_boxes: [],
        }
    }

    add_box(event){
        const new_state = Object.assign({}, this.state)

        const var_box_data = {
            author: 'Luis Herrera',
            photo:  'https://randomuser.me/api/portraits/men/'+Math.floor((Math.random() * 99) + 1)+'.jpg',
            author_email: 'l.herrera.garnica@gmail.com',
            author_msj: event.target.value,
        }
        new_state.list_boxes.push(var_box_data)

        this.setState({
            list_boxes: new_state.list_boxes,
        })
    }

    render() {
        const {list_boxes}=this.state

        const styles = {
            'paddingTop': '3em',
        }

        return (
            <div className="App">
                <div className="container">
                    <div className="columns is-centered" style={styles}>
                        <div className="column is-half">

                            <ImputText add_box={ this.add_box.bind(this) }/>
                            {list_boxes.map( (data, idx) => <Box key={idx} box_data={data}  /> ).reverse()}

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
