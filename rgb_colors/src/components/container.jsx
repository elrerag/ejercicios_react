import React from 'react'

import logo from '../logo.svg';

import {Input} from './input'

export class Container extends React.Component{
    state = {
        color: {r1: '0',r2: '0',g1: '0',g2: '0',b1: '0',b2: '0'},
    }
    
    handleChange(event){
        const {color} = this.state
    
        const code = event.target.value
        const key = event.target.name

        this.setState({ color:{ ...color, key:code } })
        debugger
    
    }
    
    render() {
        
        const {color} = this.state
    
        const rgb_code = '#'+color.r1+color.r2+color.g1+color.g2+color.b1+color.b2
    
        const inuts_values = {
            'r1': 'R1',
            'r2': 'R2',
            'g1': 'G1',
            'g2': 'G2',
            'b1': 'B2',
            'b2': 'B2',
        }
    
        console.log(rgb_code)
    
        const style = {
            backgroundColor: rgb_code
        }
    
        return(
            <div className="container" >
            <nav className="panel">
                <p className="panel-heading">
                    RGB Colors... <img src={logo} className="App-logo" alt="logo" />
                </p>
                <div className="panel-block">
                    <div className="columns">
                        {
                            Object.keys(inuts_values).map( 
                                (key, idx) =>
                                <div className="column" key={idx}>
                                    <Input 
                                        name={key}
                                        handleChange={this.handleChange.bind(this)}
                                        label={inuts_values[key]}
                                    />
                                </div>
                                )
                            }
                    </div>
                </div>
            </nav>
    
            <div className="box" style={style} >
                <article className="media" >
                </article>
            </div>
    
        </div>
        )
    }

}

export default Container