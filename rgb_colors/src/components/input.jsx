import React from 'react'

export class Input extends React.Component {

    render(){

        const {name, handleChange, label} = this.props
        const code_values = [0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f']
        return(
        
        <div className="field" >
        <label className="label">{label}</label>
            <div className="control">
                <div className="select is-primary">
                <select onChange={handleChange}  name={name}>
                    { code_values.map((x, idx) => <option key={idx} value={x}>{x}</option>) }
                </select>
                </div>
            </div>
        </div>
        )
    }
}

export default Input
